// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const CONFIG = {
  API_KEY: 'AIzaSyBxDZeuDBHcrYvc9pwyqEI82hnlvWtk9dE',
  PROJECT_ID: 'recettes-78697',
  STORAGE_BUCKET: 'gs://recettes-78697.appspot.com'

};

export const environment = {
  production: false,
  firebase: {
    apiKey: CONFIG.API_KEY,
    authDomain: CONFIG.PROJECT_ID+".firebaseapp.com",
    storageBucket: CONFIG.STORAGE_BUCKET
  }
};
