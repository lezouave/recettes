export class Ingredient {
  constructor(
    public id: string,
    public name: string,
    public category: IngredientCategory,
    public amount?: string,
    ) {
  }
}

export enum IngredientCategory {
  Légumes,
  Viandes,
  Fruits,
  Boissons,
  Epices
}

export namespace IngredientCategory {
  export function values() {
    return Object.keys(IngredientCategory).filter(
      (type) => isNaN(<any>type) && type !== 'values'
    );
  }
}
