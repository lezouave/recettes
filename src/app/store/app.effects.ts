import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

import {Store} from "@ngrx/store";
import {Actions, Effect} from "@ngrx/effects";

import * as AppActions from "./app.actions";
import * as fromApp from "./app.reducers";

import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/forkJoin';

import * as RecipeActions from '../recipes/store/recipe.actions';
import * as IngredientListActions from '../ingredient-list/store/ingredient-list.actions';


const DB_URL = 'https://recettes-78697.firebaseio.com/recettes.json';

@Injectable()
export class AppEffects {

  @Effect()
  fetchData = this.actions$
    .ofType(AppActions.FETCH_DATA)
    .switchMap((action: AppActions.FetchData) => {
        return this.http.get<any>(DB_URL, {
          observe: 'body',
          responseType: 'json'
        });
      }
    )
    .mergeMap(
      data => {
        //TODO: gérer si aucune recette ou ingredient en base.
        const recipes = data.recipes.recipes;
        const ingredients = data.ingredients;
        return [
          {
            type: RecipeActions.SET_RECIPES,
            payload: recipes
          },
          {
            type: IngredientListActions.SET_INGREDIENTS,
            ingredients: ingredients
          }
        ];
      }
    );


  @Effect({dispatch: false})
  storeData = this.actions$
    .ofType(AppActions.STORE_DATA)
    .withLatestFrom(this.store.select(state => state.recipes))
    .withLatestFrom(this.store.select(state => state.ingredientList))
    .switchMap(([[action, recipes], ingredients]) => {
      return this.http.put(DB_URL,
        {ingredients: ingredients.entities, recipes: recipes},
        {
          observe: 'body'
          // headers: headers
        });
    });

  constructor(private actions$: Actions,
              private http: HttpClient,
              private store: Store<fromApp.FeaturesState>) {
  }

}
