import {ActionReducerMap} from "@ngrx/store";

import * as fromAuth from '../auth/store/auth.reducers';
import {RecipeState} from "../recipes/store/recipe.reducers";
import {ShoppingListState} from "../shopping-list/store/shopping-list.reducers";
import {IngredientListState} from "../ingredient-list/store/ingredient-list.reducers";


export interface AppState {
  auth: fromAuth.AuthState;
}


export const reducers: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer
};


export interface FeaturesState extends AppState {
  recipes: RecipeState;
  shoppingList: ShoppingListState;
  ingredientList: IngredientListState;
}


