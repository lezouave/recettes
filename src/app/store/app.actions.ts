import {Action} from "@ngrx/store";

export const STORE_DATA = 'STORE_DATA';
export const FETCH_DATA = 'FETCH_DATA';

export class StoreData implements Action {
  readonly type = STORE_DATA;
}

export class FetchData implements Action {
  readonly type = FETCH_DATA;
}

export type AppActionsType =
  StoreData |
  FetchData
  ;
