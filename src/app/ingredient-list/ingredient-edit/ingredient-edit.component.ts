import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Store} from "@ngrx/store";
import {NgForm} from "@angular/forms";

import {Ingredient, IngredientCategory} from "../../shared/ingredient.model";
import {Subscription} from "rxjs/Subscription";
import * as IngredientListActions from '../store/ingredient-list.actions';
import * as fromIng from '../store/ingredient-list.reducers';
import {UUID} from 'angular2-uuid';
import {FilterService} from "../filter.service";


@Component({
  selector: 'app-ingredient-edit',
  templateUrl: './ingredient-edit.component.html',
  styleUrls: ['./ingredient-edit.component.css']
})
export class IngredientEditComponent implements OnInit, OnDestroy {
  @ViewChild('ingF') ingredientListForm: NgForm;

  subscription: Subscription;
  editMode = false;
  editedIngredientId: string;
  IngredientCategories = IngredientCategory;
  private ingredientCategory = null;
  private ingredientName: string;


  constructor(private store: Store<fromIng.IngredientListState>, private filterService: FilterService) {
  }

  ngOnInit() {

    this.store.select(fromIng.getEditedIngredient).subscribe(
        editedIngredient => {
          if (editedIngredient && editedIngredient !== null) {
            this.editedIngredientId = editedIngredient.id;
            this.editMode = true;
            this.ingredientListForm.setValue({
              ingredientName: editedIngredient.name,
              ingredientCategory: editedIngredient.category,
            });
          } else {
            this.editMode = false;
          }
        }
      );
  }


  onSubmit(form: NgForm) {

    const newIngredient = new Ingredient(UUID.UUID(), this.ingredientName, this.ingredientCategory);

    // Edition de l'ingredient
    if (this.editMode) {
      this.store.dispatch(new IngredientListActions.UpdateIngredient(this.editedIngredientId, newIngredient));
    } else {
      // Ajout d'un ingredient
      this.store.dispatch(new IngredientListActions.AddIngredient(newIngredient));
    }

    this.onClear();
  }

  onClear() {
    this.ingredientListForm.reset();
    this.editMode = false;
    this.store.dispatch(new IngredientListActions.StopEditIngredient());
    this.filterByCategory(null, null);
  }

  onDelete() {
    this.store.dispatch(new IngredientListActions.DeleteIngredient(this.editedIngredientId));
    this.onClear();
  }


  filterByName(searchName, searchCategory) {
    this.filterService.filterByName(searchName, searchCategory);
  }

  filterByCategory(searchName, searchCategory) {
    this.filterService.filterByCategory(searchName, searchCategory);
  }

  ngOnDestroy() {
    this.store.dispatch(new IngredientListActions.StopEditIngredient());
    this.subscription.unsubscribe();
  }

}
