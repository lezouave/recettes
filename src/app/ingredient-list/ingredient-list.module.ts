import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {IngredientListComponent} from "./ingredient-list.component";
import {SharedModule} from "../shared/shared.module";
import {StoreModule} from "@ngrx/store";
import {IngredientListRoutingModule} from "./ingredient-list-routing.module";
import {ingredientListReducer} from "./store/ingredient-list.reducers";
import {IngredientEditComponent} from "./ingredient-edit/ingredient-edit.component";


@NgModule({
  declarations: [
    IngredientListComponent,
    IngredientEditComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IngredientListRoutingModule,
    StoreModule.forFeature('ingredientList', ingredientListReducer),
  ],
  exports: [
    IngredientListComponent,
    IngredientEditComponent
  ]
})
export class IngredientListModule {

}
