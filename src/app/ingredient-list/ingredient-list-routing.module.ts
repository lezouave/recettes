import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {ROUTER_PATHNAMES} from "../router-pathnames";
import {IngredientListComponent} from "./ingredient-list.component";


const shoppingListRoutes: Routes = [
  { path: ROUTER_PATHNAMES.INGREDIENT_LIST, component: IngredientListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(shoppingListRoutes)],
  exports: [RouterModule]
})
export class IngredientListRoutingModule {
}
