import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";

import * as fromApp from '../store/app.reducers';
import * as IngredientListActions from "./store/ingredient-list.actions";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Ingredient, IngredientCategory} from "../shared/ingredient.model";

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {FilterService} from "./filter.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css'],
  animations: [
    trigger('list1', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(400)]),
      transition('* => void', [
        animate(400, style({
          transform: 'translateX(-100px)',
          opacity: 0
        }))
      ])
    ]),
  ]
})
export class IngredientListComponent implements OnInit, OnDestroy {
  ingredientList: Ingredient[];
  subscription: Subscription;
  IngredientCategories = IngredientCategory;
  editedIngredient: string;



  constructor(private store: Store<fromApp.FeaturesState>, private filterService: FilterService) {
  }

  ngOnInit() {
    this.subscription = this.filterService.getIngredients().subscribe(
      ingredients => {
        this.ingredientList = ingredients;
      }
    );

    this.store.select('ingredientList').subscribe(
      ingredientState => {
        this.editedIngredient =  ingredientState.editedIngredientId;
      }
    );
  }


  onEditItem(ingredientId: string) {
    this.store.dispatch(new IngredientListActions.StartEditIngredient(ingredientId));
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }



}
