import {Ingredient} from "../../shared/ingredient.model";
import * as IngredientListActions from "./ingredient-list.actions";
import {EntityState} from "@ngrx/entity";
import {ingredientListAdapter} from "../ingredient-list.adapter";
import {createFeatureSelector, createSelector} from "@ngrx/store";


export interface IngredientListState extends EntityState<Ingredient> {
  editedIngredientId: string | null;
}

const initialState: IngredientListState = ingredientListAdapter.getInitialState({
  editedIngredientId: null
})

export function ingredientListReducer(state: IngredientListState = initialState, action: IngredientListActions.IngredientListActionsType) {
  switch (action.type) {
    case IngredientListActions.SET_INGREDIENTS:
      // on récupère l'objet correspondant au model Ingredient
      const ingredientsMod = Object.keys(action.ingredients).map(
        key => action.ingredients[key]
      );
      return ingredientListAdapter.addAll(ingredientsMod, state);
    case IngredientListActions.ADD_INGREDIENT: {
      return ingredientListAdapter.addOne(action.ingredient, state);
    }
    case IngredientListActions.UPDATE_INGREDIENT: {
      return ingredientListAdapter.updateOne({
        id: action.id,
        changes: action.changes
      }, state);
    }
    case IngredientListActions.DELETE_INGREDIENT: {
      return ingredientListAdapter.removeOne(action.id, state);
    }
    case IngredientListActions.START_EDIT_INGREDIENT:
      return {
        ...state,
        editedIngredientId: action.payload
      }
    case IngredientListActions.STOP_EDIT_INGREDIENT:
      return {
        ...state,
        editedIngredientId: null
      }
    default:
      return state;

  }
}

export const getIngredientListState = createFeatureSelector<IngredientListState>('ingredientList');

export const getEditedIdState = ( state: IngredientListState) => state.editedIngredientId;

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = ingredientListAdapter.getSelectors(getIngredientListState);


export const selectEditedId = createSelector(getIngredientListState, getEditedIdState);


export const getIngredients = createSelector(selectEntities,
  entities => {
    return Object.keys(entities).map(id => entities[id]);
  }
);

export const getEditedIngredient = createSelector(selectEditedId, selectEntities,
  (editedId, entities) => {
    return entities[editedId];
  }
);

export const getIngredientsAndEditedIngredient = createSelector(getIngredientListState,
  () => {
    return {
      ingredients: getIngredients,
      editedIngredient: getEditedIngredient
    };
  }
);
