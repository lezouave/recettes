import {Ingredient} from "../../shared/ingredient.model";
import {Action} from "@ngrx/store";

export const SET_INGREDIENTS = 'SET_INGREDIENTS';
export const ADD_INGREDIENT = 'ADD_INGREDIENT';
export const UPDATE_INGREDIENT = 'UPDATE_INGREDIENT';
export const DELETE_INGREDIENT = 'DELETE_INGREDIENT';
export const START_EDIT_INGREDIENT = 'START_EDIT_INGREDIENT';
export const STOP_EDIT_INGREDIENT = 'STOP_EDIT_INGREDIENT';



export class SetIngredients implements Action {
  readonly type = SET_INGREDIENTS;

  constructor(public ingredients: Ingredient[]) {
  }
}

export class AddIngredient implements Action {
  readonly type = ADD_INGREDIENT;

  constructor(public ingredient: Ingredient) {
  }
}


export class UpdateIngredient implements Action {
  readonly type = UPDATE_INGREDIENT;

  constructor(public id: string,
              public changes: Partial<Ingredient>) {
  }
}

export class DeleteIngredient implements Action {
  readonly type = DELETE_INGREDIENT;

  constructor(public id: string) {
  }
}

export class StartEditIngredient implements Action {
  readonly type = START_EDIT_INGREDIENT;

  constructor(public payload: string) {
  }
}

export class StopEditIngredient implements Action {
  readonly type = STOP_EDIT_INGREDIENT;

}



export type IngredientListActionsType =
  SetIngredients |
  AddIngredient |
  UpdateIngredient |
  DeleteIngredient |
  StartEditIngredient |
  StopEditIngredient ;
