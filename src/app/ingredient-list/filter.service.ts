import {Injectable} from "@angular/core";
import {IngredientCategory} from "../shared/ingredient.model";
import {isUndefined} from "util";
import {Store} from "@ngrx/store";
import * as fromIngredient from "./store/ingredient-list.reducers";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class FilterService {
  private ingredientsSubject = new BehaviorSubject<any>(
    this.store.select(fromIngredient.getIngredients)
  );

  constructor(private store: Store<fromIngredient.IngredientListState>) {
  }

  filterByName(searchName: string, searchCategory: IngredientCategory) {
    // Si aucun filtre sur le nom on utilise le filtre de categorie
    if (searchName === null || isUndefined(searchName) || searchName === '') {
      this.filterByCategory(null, searchCategory);
    } else {
      // filtre sur le nom
      return this.ingredientsSubject.next(
        this.store.select(fromIngredient.getIngredients)
          .map(
            (ingredients) => ingredients.filter(
              ingredient => {
                return (ingredient.name.toLowerCase().indexOf(searchName.toLowerCase()) > -1);
              }
            )
          ));
    }
  }

  filterByCategory(searchName: string, searchCategory: IngredientCategory) {
    // Uniquement si le filtre sur le nom n'est pas utilisé on filtre sur la catégorie
    if (searchName === null || isUndefined(searchName) || searchName === '') {
      // pour toutes les catégories
      if (searchCategory === null) {
        this.ingredientsSubject.next(
          this.store.select(fromIngredient.getIngredients)
        );
      } else {
        this.ingredientsSubject.next(
          this.store.select(fromIngredient.getIngredients)
            .map(
              (ingredients) => ingredients.filter(
                ingredient => {
                  return (ingredient.category === searchCategory);
                }
              )
            )
        );
      }
    } else {
      // Si filtre nom, prioritaire
      this.filterByName(searchName, searchCategory);
    }
  }

  getIngredients() {
    return this.ingredientsSubject.asObservable();
  }

}
