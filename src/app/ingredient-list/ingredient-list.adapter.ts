import {Ingredient} from "../shared/ingredient.model";
import {createEntityAdapter} from "@ngrx/entity";


export const ingredientListAdapter = createEntityAdapter<Ingredient>();


