import {Component, OnInit} from '@angular/core';
import {ROUTER_PATHNAMES} from "../../router-pathnames";
import {Store} from "@ngrx/store";
import * as fromApp from "../../store/app.reducers";
import * as fromAuth from "../../auth/store/auth.reducers"
import * as AuthActions from "../../auth/store/auth.actions"
import * as AppActions from "../../store/app.actions";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  authState: Observable<fromAuth.AuthState>;

  routerPathnames = ROUTER_PATHNAMES;

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.authState = this.store.select('auth');
    this.store.dispatch(new AppActions.FetchData());
  }

  onSaveData() {
   this.store.dispatch(new AppActions.StoreData());
  }

  onFetchData() {
    this.store.dispatch(new AppActions.FetchData());
  }

  onLogout() {
    this.store.dispatch(new AuthActions.LogOut());
  }

}
