import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs/Observable";
import * as ShoppingListActions from "./store/shopping-list.actions";
import * as fromApp from '../store/app.reducers';
import {ShoppingListState} from "./store/shopping-list.reducers";
import {animate, state, style, transition, trigger} from "@angular/animations";


@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
  animations: [
    trigger('list1', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(400)]),
      transition('* => void', [
        animate(400, style({
          transform: 'translateX(-100px)',
          opacity: 0
        }))
      ])
    ]),
  ]
})
export class ShoppingListComponent implements OnInit {
  shoppingListState: Observable<ShoppingListState>;

  constructor(private store: Store<fromApp.FeaturesState>) {
  }

  ngOnInit() {
    this.shoppingListState = this.store.select('shoppingList');
  }


  onEditItem(index: number) {
    this.store.dispatch(new ShoppingListActions.StartEdit(index));
  }

}
