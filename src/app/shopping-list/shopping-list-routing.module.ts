import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {ShoppingListComponent} from "./shopping-list.component";
import {ROUTER_PATHNAMES} from "../router-pathnames";


const shoppingListRoutes: Routes = [
  { path: ROUTER_PATHNAMES.SHOPPING_LIST, component: ShoppingListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(shoppingListRoutes)],
  exports: [RouterModule]
})
export class ShoppingListRoutingModule {
}
