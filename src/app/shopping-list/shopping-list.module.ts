import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {ShoppingListComponent} from "./shopping-list.component";
import {ShoppingEditComponent} from "./shopping-edit/shopping-edit.component";
import {SharedModule} from "../shared/shared.module";
import {ShoppingListRoutingModule} from "./shopping-list-routing.module";
import {StoreModule} from "@ngrx/store";
import {shoppingListReducer} from "./store/shopping-list.reducers";
import {FilterService} from "../ingredient-list/filter.service";

@NgModule({
  declarations: [
    ShoppingListComponent,
    ShoppingEditComponent
  ],
  providers: [FilterService],
  imports: [
    CommonModule,
    SharedModule,
    ShoppingListRoutingModule,
    StoreModule.forFeature('shoppingList', shoppingListReducer),
  ]
})
export class ShoppingListModule {

}
