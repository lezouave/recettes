import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {SigninComponent} from "./signin/signin.component";
import {SignupComponent} from "./signup/signup.component";
import {ROUTER_PATHNAMES} from "../router-pathnames";


const authRoutes: Routes = [
  { path: ROUTER_PATHNAMES.SIGNUP, component: SignupComponent },
  { path: ROUTER_PATHNAMES.SIGNIN, component: SigninComponent }
];

@NgModule({
  imports: [RouterModule.forChild(authRoutes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
