import {Router} from "@angular/router";
import {Actions, Effect} from "@ngrx/effects";
import {Injectable} from "@angular/core";
import * as AuthActions from "./auth.actions";
import {fromPromise} from "rxjs/observable/fromPromise";
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class AuthEffects {
  @Effect()
  authSignUp = this.actions$
    .ofType(AuthActions.TRY_SIGN_UP)
    .map((action: AuthActions.TrySignUp) => {
      return action.payload;
    })
    .switchMap((authData: { username: string, password: string }) => {
      return fromPromise(firebase.auth().createUserWithEmailAndPassword(authData.username, authData.password));
    })
    .switchMap(() => {
      localStorage.setItem('user', firebase.auth().currentUser.email);
      return fromPromise(firebase.auth().currentUser.getIdToken(false));
    })
    .mergeMap((token) => {
      this.router.navigate(['/']);
      localStorage.setItem('token', token);
      return [
        {
          type: AuthActions.SIGN_UP
        },
        {
          type: AuthActions.SET_TOKEN,
          payload: token
        }
      ];
    })
  ;

  @Effect()
  authSignIn = this.actions$
    .ofType(AuthActions.TRY_SIGN_IN)
    .map((action: AuthActions.TrySignIn) => {
      return action.payload;
    })
    .switchMap((authData: { username: string, password: string }) => {
      return fromPromise(firebase.auth().signInWithEmailAndPassword(authData.username, authData.password));
    })
    .switchMap(() => {
      localStorage.setItem('user', firebase.auth().currentUser.email);
      return fromPromise(firebase.auth().currentUser.getIdToken(false));
    })
    .mergeMap((token) => {
      this.router.navigate(['/']);
      localStorage.setItem('token', token);
      return [
        {
          type: AuthActions.SIGN_IN
        },
        {
          type: AuthActions.SET_TOKEN,
          payload: token
        }
      ];
    });

  @Effect({dispatch: false})
  authLogout = this.actions$
    .ofType(AuthActions.LOG_OUT)
    .do(() => {
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      this.router.navigate(['/']);
    });

  constructor(private actions$: Actions, private router: Router) {
  }
}
