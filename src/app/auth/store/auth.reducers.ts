import * as AuthActions from './auth.actions';

export interface AuthState {
  token: string;
  authenticated: boolean
}

const initialState: AuthState = {
  token: null,
  authenticated: false
}

export function authReducer (state = initialState, action: AuthActions.AuthActionsType) {
  switch (action.type) {
    case AuthActions.LOAD_USER:
      return {
        ...state,
        token: action.payload,
        authenticated: true
      }
    case AuthActions.SIGN_UP:
    case AuthActions.SIGN_IN:
      return {
        ...state,
        authenticated: true
      }
    case AuthActions.LOG_OUT:
      return {
        ...state,
        token: null,
        authenticated: false
      }
    case AuthActions.SET_TOKEN:
      return {
        ...state,
        token: action.payload
      }
    default:
      return state;
  }
}
