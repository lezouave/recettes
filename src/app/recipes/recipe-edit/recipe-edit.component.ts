import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";

import * as RecipeActions from "../store/recipe.actions";
import * as fromRecipe from "../store/recipe.reducers";
import * as fromApp from "../../store/app.reducers"
import {AngularFireStorage} from "angularfire2/storage";
import {UUID} from 'angular2-uuid';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode = false;
  recipeForm: FormGroup;
  imageId: string;
  imageView = '';
  ingredients: Observable<any>;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store<fromApp.FeaturesState>,
              private storage: AngularFireStorage,
              ) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.initForm();
      }
    );
    this.ingredients = this.store.select(state => state.ingredientList);
  }

  onSubmit() {
    if (this.editMode) {
      this.store.dispatch(new RecipeActions.UpdateRecipe({index: this.id, updatedRecipe: this.recipeForm.value}));
    } else {
      const recipe = this.recipeForm.value;
      recipe.imageId = this.imageId;

      this.store.dispatch(new RecipeActions.AddRecipe(recipe));
    }
    this.onCancel();
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  getControls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

  onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, Validators.required)
      })
    );
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

  private initForm() {
    let recipeName = '';
    let recipeDescription = '';
    let recipeIngredients = new FormArray([]);

    if (this.editMode) {
      this.store.select('recipes')
        .take(1)
        .subscribe((recipeState: fromRecipe.RecipeState) => {
          const recipe = recipeState.recipes[this.id];
          recipeName = recipe.name;
          recipeDescription = recipe.description;
          if (recipe['ingredients']) {
            for (let ingredient of recipe.ingredients) {
              recipeIngredients.push(
                new FormGroup({
                  'name': new FormControl(ingredient.name),
                  'amount': new FormControl(ingredient.amount)
                })
              );
            }
          }
        });
    }

    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required ),
      'description': new FormControl(recipeDescription, Validators.required),
      'ingredients': recipeIngredients
    });
  }

  uploadFile(event) {
    // enregistrement de l'ID de l'image
    const uuid = UUID.UUID();
    this.imageId = uuid;
    const file = event.target.files[0];

    // enregistrement dans le storage firebase
    this.storage.upload('recettes/' + uuid, file);

    // prévisualisation en base 64
    const reader = new FileReader();
    reader.onload = () => {
      this.imageView = reader.result;
    };

    if (file) {
      reader.readAsDataURL(file);
    }


  }

}
