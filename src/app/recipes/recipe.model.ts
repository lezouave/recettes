import {Ingredient} from "../shared/ingredient.model";

export class Recipe {
  constructor(
    public name: string,
    public description: string,
    public  imageId: string,
    public imagePath: string,
    public ingredients: Ingredient[]
  ) {}
}
