import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Actions, Effect} from "@ngrx/effects";

import * as RecipeActions from "./recipe.actions";
import {Recipe} from "../recipe.model";
import {FirebaseApp} from "angularfire2";

import {fromPromise} from "rxjs/observable/fromPromise";

import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/forkJoin';
import {Observable} from "rxjs/Observable";


const DB_URL = 'https://recettes-78697.firebaseio.com/recettes/recipes/recipes.json';

@Injectable()
export class RecipeEffects {

  @Effect()
  recipeFetch = this.actions$
    .ofType(RecipeActions.FETCH_RECIPES)
    .switchMap((action: RecipeActions.FetchRecipes) => {
        return this.http.get<Recipe[]>(DB_URL, {
          observe: 'body',
          responseType: 'json'
        });
      }
    )
    .switchMap(
      (recipes: Recipe[]) => {
        return Observable.forkJoin(
          recipes.map((recipe: Recipe) => {
            return fromPromise(this.firebaseApp.storage().ref().child('recettes/' + recipe.imageId).getDownloadURL())
              .map((url) => {
                  recipe.imagePath = url;
                  if (!recipe['ingredients']) {
                    recipe['ingredients'] = [];
                  }
                  return recipe;
                }
              );
          })
        );
      }
    )
    .map(
      (recipes) => {
        return {
          type: RecipeActions.SET_RECIPES,
          payload: recipes
        };
      }
    );

  constructor(private actions$: Actions,
              private http: HttpClient,
              private firebaseApp: FirebaseApp) {
  }

}
