import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RecipesComponent} from "./recipes.component";
import {RecipeStartComponent} from "./recipe-start/recipe-start.component";
import {RecipeEditComponent} from "./recipe-edit/recipe-edit.component";
import {RecipeDetailComponent} from "./recipe-detail/recipe-detail.component";
import {AuthGuardService} from "../auth/auth-guard.service";
import {SharedModule} from "../shared/shared.module";
import {ROUTER_PATHNAMES} from "../router-pathnames";


const recipesRoutes: Routes = [
  {
    path: ROUTER_PATHNAMES.RECIPES, component: RecipesComponent, children: [
      {path: '', component: RecipeStartComponent},
      {path: ROUTER_PATHNAMES.CREATE, component: RecipeEditComponent, canActivate: [AuthGuardService]},
      {path: ':id', component: RecipeDetailComponent},
      {path: ':id/' + ROUTER_PATHNAMES.EDIT, component: RecipeEditComponent, canActivate: [AuthGuardService]},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(recipesRoutes), SharedModule],
  exports: [RouterModule],
  providers: [
    AuthGuardService
  ]
})
export class RecipesRoutingModule {
}
