import {Component, Input, OnInit} from '@angular/core';
import {Recipe} from "../../recipe.model";
import {FirebaseApp} from "angularfire2";

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe: Recipe;
  @Input() index: number;
  image = './assets/images/loader.svg';


  constructor(private firebaseApp: FirebaseApp) {
  }

  ngOnInit() {
    this.firebaseApp.storage().ref().child('recettes/'+ this.recipe.imageId).getDownloadURL().then(
      (url) => {
        this.image = url;
      }
    );
  }


}
