import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs/Observable";
import * as fromAuth from "../../auth/store/auth.reducers"
import * as fromRecipe from "../../recipes/store/recipe.reducers"
import * as fromApp from "../../store/app.reducers"

import {ROUTER_PATHNAMES} from "../../router-pathnames";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  authState: Observable<fromAuth.AuthState>;
  recipeState: Observable<fromRecipe.RecipeState>;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<fromApp.FeaturesState>) {
  }

  ngOnInit() {
    this.authState = this.store.select('auth');
    this.recipeState = this.store.select('recipes');
  }

  onNewRecipe() {
    this.router.navigate([ROUTER_PATHNAMES.CREATE], { relativeTo: this.route});
  }


}
