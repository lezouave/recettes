import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";

import {Store} from "@ngrx/store";
import 'rxjs/add/operator/take';

import {ROUTER_PATHNAMES} from "../../router-pathnames";
import * as ShoppingListActions from "../../shopping-list/store/shopping-list.actions";
import {Observable} from "rxjs/Observable";
import * as fromAuth from "../../auth/store/auth.reducers";
import * as fromRecipe from "../../recipes/store/recipe.reducers";
import * as RecipeActions from "../../recipes/store/recipe.actions";
import * as fromApp from "../../store/app.reducers"


@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  authState: Observable<fromAuth.AuthState>;
  recipeState: Observable<fromRecipe.RecipeState>;
  id: number;
  image;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<fromApp.FeaturesState>) { }

  ngOnInit() {
    this.authState = this.store.select('auth');
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.image = +params['image'];
        this.recipeState = this.store.select('recipes');
      }
    );

  }

  onAddToShoppingList() {
    this.store.select('recipes')
      .take(1)
      .subscribe( (recipeState: fromRecipe.RecipeState) => {
        this.store.dispatch(new ShoppingListActions.AddIngredients(recipeState.recipes[this.id].ingredients));
      });
  }

  onEditRecipe() {
    this.router.navigate([ROUTER_PATHNAMES.EDIT], {relativeTo: this.route});
  }

  onDeleteRecipe() {
    this.store.dispatch(new RecipeActions.DeleteRecipe(this.id));
    this.router.navigate([ROUTER_PATHNAMES.RECIPES]);
  }




}
