import {Component, OnInit} from '@angular/core';
import * as fromApp from './store/app.reducers';
import * as AuthActions from './auth/store/auth.actions';
import {Store} from "@ngrx/store";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private store: Store<fromApp.AppState>) {

  }
  ngOnInit() {

    if(localStorage.getItem('user') && localStorage.getItem('token')) {
      this.store.dispatch(new AuthActions.LoadUser(localStorage.getItem('token')));
    }
  }
}
