export const ROUTER_PATHNAMES = {
  RECIPES: 'recettes',
  CREATE: 'creation',
  EDIT: 'edition',
  SHOPPING_LIST: 'liste-courses',
  INGREDIENT_LIST: 'ingredients',
  AUTH: 'membre',
  SIGNIN: 'connexion',
  SIGNUP: 'creation-compte'
};
