import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {environment} from "../environments/environment";

import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "./shared/shared.module";
import {CoreModule} from "./core/core.module";
import {StoreModule} from "@ngrx/store";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {reducers} from "./store/app.reducers";
import {EffectsModule} from "@ngrx/effects";
import {AuthEffects} from "./auth/store/auth.effects";
import {StoreRouterConnectingModule} from "@ngrx/router-store";
import {AuthModule} from "./auth/auth.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ShoppingListModule} from "./shopping-list/shopping-list.module";
import {RecipesModule} from "./recipes/recipes.module";
import {AngularFireModule} from "angularfire2";
import {AngularFireStorageModule} from "angularfire2/storage";
import {IngredientListModule} from "./ingredient-list/ingredient-list.module";
import {AppEffects} from "./store/app.effects";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    AuthModule,
    ShoppingListModule,
    IngredientListModule,
    RecipesModule,
    StoreModule.forRoot(reducers),
    !environment.production ? StoreDevtoolsModule.instrument({}) : [],
    EffectsModule.forRoot([AuthEffects, AppEffects]),
    StoreRouterConnectingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
